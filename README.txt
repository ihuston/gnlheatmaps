**********************
README for gnlheatmaps
**********************

Author: Ian Huston
License: 3-clause BSD (See LICENSE.txt file)

Introduction
------------
gnlheatmaps contains a helper module heatmaps.py which produces heatmap images
for the coefficients of the f_NL, g_NL and tau_NL observational quantities 
associated with non-Gaussianity in the early universe.

Full details of the coefficient functions can be found in the arXiv article
"Large trispectrum in two-field slow-roll inflation" available at 
http://arxiv.org/abs/1203.6844 .

Requirements
------------
* Python
* Matplotlib
* Numpy


Usage
-----
In order to generate the images used in the paper just run the script in the
directory you wish to store the images::
	
	$ python heatmaps.py
	
Use the ``--help`` command line option to list the options available. 
These include

-h, --help            
	show this help message and exit
	
-d DIR, --dir=DIR     
	directory to store figures, default is the current directory
    
-f FILEFORMAT, --format=FILEFORMAT
	extension of format of created files, default is png
	
-s SUFFIX, --suffix=SUFFIX
	suffix to add to file names, default is empty string
	
--rasterized         
	toggle whether to plot rasterized figures which can reduce file size
	
--small               
	toggle whether small figures should be made
 
Tips
----
* Vector graphics of the heatmaps (including pdf and eps formats) can be 
  extremely large due to the number of lines in the images. In order to reduce
  the file size with no apparent loss of quality it is possible to specify that
  rasterized images are used for the complicated parts of the image, while vector
  graphics are still used for the axes and labels.
* The number and type of possible output formats are determined by the 
  matplotlib installation.

License
-------
This script is released under a standard 3-clause BSD license. The license
text is available in the LICENSE.txt file.

Acknowledgements
----------------
This script was created by Ian Huston while working at Queen Mary, University
of London. Ian Huston is supported by the Science and Technology Facilities 
Council under grant ST/J001546/1.
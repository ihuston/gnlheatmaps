''' heatmaps - Plot heatmaps of functions used in f_NL, g_NL and tau_NL 
expressions. 

Author: Ian Huston

Please see accompanying LICENSE.txt file for license information
'''


from __future__ import division

from matplotlib import pylab as P
import numpy as np
from numpy import cos, sin, tan
import os, sys
import optparse

  

def gamma(a,s):
    return cos(a)**4*sin(s)**2 + sin(a)**4*cos(s)**2

#Sum separable
def f_1(a,s):
    return sin(2*s)**2/(2*gamma(a,s))

def f_2(a,s):
    return (cos(a)**6*sin(s)**4 + sin(a)**6*cos(s)**4)/(gamma(a,s)**2)

def f_3(a,s):
    return sin(2*s)*(cos(a)**6*sin(s)**2 - sin(a)**6*cos(s)**2)/(2*gamma(a,s)**2)

def f(a,s):
    return sin(2*a)**2*(cos(a)**2 - cos(s)**2)**2/(4*gamma(a,s)**2)

def Omega(a,t,W,Wstar):
    return (W/Wstar)**2 * (sin(2*t)/sin(2*a))**2

#Tau nl
def t_1(a,s):
    return (cos(a)**8*sin(s)**6 + sin(a)**8*cos(s)**6)/(gamma(a,s)**3)

def t_2(a,s):
    return sin(2*s)*(cos(a)**8*sin(s)**4 - sin(a)**8*cos(s)**4)/(gamma(a,s)**3)

def t_3(a,s):
    return f_1(a,s)*(cos(a)**8*sin(s)**2 + sin(a)**8*cos(s)**2)/(2*gamma(a,s)**2)

def t_4(a,s):
    return 2*f_1(a,s)*f_2(a,s)

def t_5(a,s):
    return 2*f_1(a,s)*f_3(a,s)
    
def t_6(a,s):
    return f_1(a,s)**2

def t_7(a,s):
    return 4*f(a,s)*(cos(a)**2*sin(s)**2 + sin(a)**2*cos(s)**2)/(gamma(a,s))

def t_8(a,s):
    return -sin(2*s)*sin(2*a)**2*(cos(a)**2 - cos(s)**2)/(2*gamma(a,s)**2)

def t_9(a,s):
    return 4*f_1(a,s)*f(a,s)

def t(a,s):
    return f(a,s)*sin(2*a)**2/(4*gamma(a,s))

def g_3(a,s):
    return -f(a,s)*(cos(a)**2-cos(s)**2)/(2*gamma(a,s))

def g_1(a,s):
    return g_3(a,s)*sin(2*a)

def g_2(a,s):
    return g_3(a,s)*cos(2*a)

def g_4(a,s):
    return 0.25*(t_3(a,s)*sin(2*s)*cos(2*s)-t_2(a,s))

def C(a,s):
    return gamma(a,s)/(cos(a)**2 - cos(s)**2)**2


#####################################################

def get_2d_arrays(f, npoints=300, alim=None, tlim=None):
    """Plot function f(alpha,theta) for alpha and theta in [0,2pi]
    
    Arguments
    ---------
    f: function
       Function to plot, with signature f(alpha, theta). Should be capable of 
       taking ndarrays as input
    """
    if alim is None:
        alim = [0,np.pi/2]
    if tlim is None:
        tlim = [0,np.pi/2]
    a = np.linspace(alim[0], alim[1], npoints)
    t = np.linspace(alim[0], alim[1], npoints)
    
    alpha, theta = np.meshgrid(a, t)
    
    Z = np.nan_to_num(f(alpha, theta))
    return alpha, theta, Z

def heatmap(f, npoints=300, maxlimit=5, minlimit=0, alim=None, tlim=None, 
            cmap=None, fname=None, small=False, rasterized=False):
    """Plot a heatmap of function f(alpha,theta) for a range of alpha and 
    theta.
    
    Parameters
    ----------
    f : function
       Function to plot, with signature f(alpha, theta). Should be capable of 
       taking ndarrays as input
       
    npoints : integer, optional 
              The number of points to divide each axis into, default is 300.
    
    maxlimit : float, optional
               The upper limit to clip values at, default is 5.0.
               
    minlimit : float, optional
               The lower limit to clip values at, default is 0.0.
               
    alim : list
           List of the lower and upper limits of the alpha variable,
           default is [0, np.pi/2].
    
    tlim : list
           List of the lower and upper limits of the theta variable,
           default is [0, np.pi/2].
                  
    cmap : colormap, optional
           Matplotlib colormap to use in plotting heatmap, default is cm.RdBu_r.
           
    fname : string, optional
            Name of the function to be placed at top of figure, default is the
            name of the function f.
            
    small : boolean, optional
            If True, applies formatting for small figures, default is False.
            
    rasterized : boolean, optional
                 If True, uses rasterized option in plotting figure, which
                 could reduce file sizes for vector file formats (pdf, eps).
                 Default is False.
                 
    Returns
    -------
    fig : matplotlib figure handle
          Figure created with required properties.
    
    See Also
    --------
    "Large trispectrum in two-field slow-roll inflation", 
    Joseph Elliston, Laila Alabidi, Ian Huston, David J. Mulryne, Reza Tavakol
    arXiv:1203.6844 http://arxiv.org/abs/1203.6844
    
    """
    if alim is None:
        alim = [0,np.pi/2]
    if tlim is None:
        tlim = [0,np.pi/2]
    if cmap is None:
        cmap = P.cm.get_cmap("RdBu_r")
    if fname is None:
        fname = r"$" + f.__name__ + r"$"
    alpha, theta, Z = get_2d_arrays(f, npoints, alim, tlim)
    np.clip(Z, minlimit, maxlimit, Z)
    
    
    fig = P.figure()
    if small:
        fig.set_size_inches((4.01,3.8))
        fig.subplots_adjust(left=0.08, bottom=0.15, right=1.0, top=0.85)
    ax = fig.gca()
    ax.set_aspect("equal")
    P.pcolormesh(alpha, theta, Z.T, vmin=minlimit, vmax=maxlimit, cmap=cmap,
                 rasterized=rasterized)
    P.colorbar()
    P.axis([tlim[0],tlim[1],alim[0],alim[1]])
    P.xlabel(r"$\theta^*$")
    P.ylabel(r"$\alpha$")
    
    P.title(fname)
    return fig

def plot_and_save(funclist=None, filenamesuffix="", filepath=None, small=False,
                  rasterized=False, fileformat="png"):
    """Plot and save heatmaps.
    
    Parameters
    ----------
    funclist : list, default includes f_3, f_4_uplifted, t_4, t_7_uplifted, 
               t_9_uplifted, g_1_uplifted and g_2_uplifted.
               List of the functions to be individually plotted and saved. 
               The format of the list is (functionname, latexname, minlimit, maxlimit,cmap).
              
    filenamesuffix : String
                     An informational string to attach after the function name
                     to the file name of the saved figure, 
                     e.g. for "-final" the filename could be "f_3-final.pdf"
    
    filepath : String, default is current directory
               Path to the directory in which to save the figures
               
    small : boolean, optional
            If True use settings for small figures, default is False.
            
    rasterized : boolean, optional
                 If True, uses rasterized option in plotting figure, which
                 could reduce file sizes for vector file formats (pdf, eps).
                 Default is False.
                 
    fileformat : string, optional
                 The extension of the format to be used, default is "png".
                 
    Returns
    -------
    filenames: List of the filenames saved
    
    See Also
    --------
    "Large trispectrum in two-field slow-roll inflation", 
    Joseph Elliston, Laila Alabidi, Ian Huston, David J. Mulryne, Reza Tavakol
    arXiv:1203.6844 http://arxiv.org/abs/1203.6844
    """
    if funclist is None:
        cmredblue = P.cm.get_cmap("RdBu_r")
        cmred = P.cm.get_cmap("Reds")
        funclist = [(f, r"$f$", 0, 10, cmred),
                    (t, r"$\tau$", 0, 10, cmred),
                    (g_1, r"$g_1$", -10, 10, cmredblue),
                    (g_4, r"$g_4$", -10, 10, cmredblue),
                    (g_3, r"$g_3$", -10, 10, cmredblue),
                    (C, r"$C$", 0, 10, cmred),
                    ]
        
    if filepath is None:
        filepath = os.getcwd()
    filepath = os.path.abspath(os.path.expanduser(os.path.expandvars(filepath)))
        
    if not os.path.exists(filepath):
        raise IOError("Path to store files does not exist")    
    
    filenames = []
    for fn, txt, minlim, maxlim, cm in funclist:
        fig = heatmap(fn, maxlimit=maxlim, minlimit=minlim, fname=txt, small=small, 
                      cmap=cm, rasterized=rasterized)

        fname = os.path.join(filepath, fn.__name__ + filenamesuffix + "." + fileformat)
        try:
            fig.savefig(fname, format=fileformat)
        except IOError:
            print "Problem saving " + fname + "file."
        else:
            filenames += [fname]
            print "Saving figure " + fname
    return filenames



def main(argv=None):
    """heatmaps.py: Create figures of heatmaps and save."""
    if not argv:
        argv = sys.argv
    
    #Parse command line options
    parser = optparse.OptionParser()
     
    parser.add_option("-d", "--dir", action="store", dest="filepath",
                        default=os.getcwd(), type="string",
                        metavar="DIR", help="directory to store figures, default=%default")

    parser.add_option("-f", "--format", action="store", dest="fileformat",
                        default="png", type="string",
                        metavar="FILEFORMAT", help="extension of format of created files, default is png")
        
    parser.add_option("-s", "--suffix", action="store", dest="suffix",
                        default="", type="string",
                        metavar="SUFFIX", help="suffix to add to file names, default is empty string")
    
    parser.add_option("--rasterized", action="store_true", dest="rasterized",
                        default=False, 
                        help="toggle whether to plot rasterized figures which can reduce file size")
    
    parser.add_option("--small", action="store_true", dest="small",
                        default=False, 
                        help="toggle whether small figures should be made")
    

    
    (options, args) = parser.parse_args(args=argv[1:])
    
    try:
        filenames = plot_and_save(filenamesuffix=options.suffix, filepath=options.filepath,
                                  fileformat=options.fileformat,
                                  small=options.small,
                                  rasterized=options.rasterized)
    except Exception, e:
        print "Error encountered, quitting!"
        print e
        return 1
    else:
        return 0

#Get root log
if __name__ == "__main__":
    sys.exit(main())
    